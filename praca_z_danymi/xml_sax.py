import xml.sax
import shutil

class StudentHandler( xml.sax.ContentHandler ):
    def __init__(self):
        self.CurrentData = ""
        self.grade = ""
        self.year = ""
    # Call when an element starts
    def startElement(self, tag, attributes):
        self.CurrentData = tag
        if tag == "student":
            id = attributes["id"]
            print("Id: ", id)

    # Call when an elements ends
    def endElement(self, tag):
        if self.CurrentData == "grade":
            print("Grade:", self.grade)
        elif self.CurrentData == "year":
            print("Year:", self.year)

    # Call when a character is read
    def characters(self, content):
        if self.CurrentData == "grade":
            self.grade = content
        elif self.CurrentData == "year":
            self.year = content
   
# create an XMLReader
parser = xml.sax.make_parser()
# turn off namepsaces
parser.setFeature(xml.sax.handler.feature_namespaces, 0)
# override the default ContextHandler
Handler = StudentHandler()
parser.setContentHandler( Handler )

#copy file
original = "original.xml"
newfile = "newfile.xml"
shutil.copyfile("original.xml", "newfile.xml")

parser.parse("newfile.xml")
print(parser.getNames())


