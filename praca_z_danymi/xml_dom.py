from xml.dom import minidom

doc = minidom.parse("original.xml")

student = doc.getElementsByTagName("student")
for s in student:
    sid = s.getAttribute("id")
    grade = s.getElementsByTagName("grade")[0]
    year = s.getElementsByTagName("year")[0]
    print("id:%s, grade:%s, year:%s" %
            (sid, grade.firstChild.data, year.firstChild.data))

    grade = s.getElementsByTagName("grade")[0].firstChild.data = 3
    with open("copy_dom.xml", "w") as xml_file:
        s.writexml(xml_file)
