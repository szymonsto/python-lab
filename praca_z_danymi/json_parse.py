import json

def add(data):
    id = input("Student id:")
    year = input("Year")

    data[id] = year
    
    with open("data.json", 'w') as outfile:
        json.dump(data, outfile)

def remove(data):
    id = input("Student id:")

    data.pop(id)

    with open("data.json", 'w') as outfile:
        json.dump(data, outfile)

try:
    file = open("data.json") 
    data = json.load(file)
    print(data)
except:
    print("No file")
    data = {}

option = input("1. Add\n2. Remove")

if option == "1":
    add(data)
else:
    remove(data)


