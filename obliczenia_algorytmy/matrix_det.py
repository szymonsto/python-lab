#Calculate determinant od the matrix

import random

dimx = 4
dimy = 4

matrix = [[random.randint(0, 10)for e in range(dimx)] for e in range(dimy)]

def copyMatrix(A):
    return [row[:] for row in A]

def calcDet(A):
    result = 0

    if len(A) == 2 and len(A[0]) == 2:
        return A[0][0] * A[1][1] - A[1][0] * A[0][1]
    
    for column in range(len(A)):

        Acopy = copyMatrix(A)[1:]

        for copyRow in range(len(Acopy)):
            Acopy[copyRow] = Acopy[copyRow][0:column] + Acopy[copyRow][column+1:]
              

        detCopyMatrix = calcDet(Acopy)
        sign = (-1)**(column%2)

        result += sign * A[0][column] * detCopyMatrix
    
    return result


print("Matrix:\n")
print(matrix)
print("Determinant:\n")
print(calcDet(matrix))