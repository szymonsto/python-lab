#Scalar product of two vectors

a = [1, 2, 12, 4]
b = [2, 4, 2, 8]

def scalarProduct(a, b):
    if len(a) != len(b):
        return 0
    
    result = 0

    for ea, eb in zip(a, b):
        result += ea * eb

    return result

print("Vectors: a = {0}, b = {1}\n".format(a, b))
print("Scalar product: " + str(scalarProduct(a, b)))