#Multiplication of matrices

import random

def scalarProduct(a, b):
    if len(a) != len(b):
        return 0
    
    result = 0

    for ea, eb in zip(a, b):
        result += ea * eb

    return result

dimx = 4
dimy = 4

matrix1 = [[random.randint(0, 10) for e in range(dimx)] for e in range(dimy)]
matrix2 = [[random.randint(0, 10) for e in range(dimx)] for e in range(dimy)]

print("Matrix 1:\n")
print(matrix1)
print("Matrix 2:\n")
print(matrix2)
print("new \n")
print(list(zip(*matrix2)))

print("asas\n")
print(matrix1[0])

result = [[int(0) for e in range(dimx)] for e in range(dimy)]


for i in range(dimx):
    for j in range(dimy):
        result[i][j] = scalarProduct(matrix1[i], list(zip(*matrix2))[:][j])
    
print("Multiplication of matrices:\n")
print(result)   