#Bubble sort random array

import random

def bubbleSort(arr, descending=False): 
    for i in range(len(arr) -1): 
        for j in range(0, len(arr) -i-1): 
            if arr[j] > arr[j+1] : 
                arr[j], arr[j+1] = arr[j+1], arr[j] 
    if descending == True:
        arr.reverse()

array = [random.random() for e in range(50)]
verifyArray = array

print("Initial array:\n")
print(array)

print("Sorted array:\n")
bubbleSort(array, True)
print(array)

if(verifyArray.sort(reverse=True) != array):
    print("Verification Failed.\nCorrect array:\n")
    print(verifyArray)




            