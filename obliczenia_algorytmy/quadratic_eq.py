#Quadratic equation in the form: y = ax^2 + bx + c
#a, b, c are command line arguments

import sys
import cmath

a, b, c = sys.argv[1:4]

a, b, c = float(a), float(b), float(c)

discriminant = (b**2) - (4 * a * c)

solution1 = (-b - cmath.sqrt(discriminant))/(2 * a)
solution2 = (-b + cmath.sqrt(discriminant))/(2 * a)

print("Solutions: {0} ; {1}".format(solution1, solution2))