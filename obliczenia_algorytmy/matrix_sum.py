#Sum of matrices

import random

dimx = 128
dimy = 128

matrix1 = [[random.random() for e in range(dimx)] for e in range(dimy)]
matrix2 = [[random.random() for e in range(dimx)] for e in range(dimy)]

print("Matrix 1:\n")
print(matrix1)
print("Matrix 2:\n")
print(matrix2)


for i in range(dimx):
    for j in range(dimy):
        matrix1[i][j] = matrix1[i][j] + matrix2[i][j]


print("Sum of matrices:\n")
print(matrix1)   