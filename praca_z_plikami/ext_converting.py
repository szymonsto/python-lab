#Script converts extensions from *.jpg to *.png
#Filename is command line argument

import os
import sys

filename = sys.argv[1]

root, ext = os.path.splitext(filename)

os.rename(filename, root + ".png")

