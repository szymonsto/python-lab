#Complex number

class Complex(object):
    def __init__(self, real, imag=0.0):
        self.real = real
        self.imag = imag

    def __add__(self, other):
        return Complex(self.real + other.real, self.imag + other.imag)

    def __sub__(self, other):
        return Complex(self.real - other.real, self.imag - other.imag)

    def __mul__(self, other):
        return Complex(self.real*other.real - self.imag*other.imag, self.imag*other.real + self.real*other.imag)

    def __truediv__(self, other):
        tmp = float(other.real**2 + other.imag**2)
        return Complex((self.real*other.real+self.imag*other.imag)/tmp, (self.imag*other.real-self.real*other.imag)/tmp)

    def __abs__(self):
        return sqrt(self.real**2 + self.imag**2)

    def __neg__(self):
        return Complex(-self.real, -self.imag)

    def __str__(self):
        return str(self.real) + "+" + str(self.imag) + "j"
