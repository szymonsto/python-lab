#Complex number calculator

import sys
from complex_numbers import Complex

def strToComplex(number):
    X = number.split("+")

    if len(X) < 2:
        return Complex(int(X[0]), 0)
    else:
        X[1] = X[1].replace("j", "")
        X[1] = X[1].replace("i", "")

        return Complex(int(X[0]), int(X[1]))

if len(sys.argv) != 4:
    print("Len: {0}: {1}".format(len(sys.argv), sys.argv))
    print("Command line arguments: <argument 1> <operator> <argument2>")
    exit()

a = strToComplex(sys.argv[1])
operator = str(sys.argv[2])
b = strToComplex(sys.argv[3])

print("A: {0}, Operator: {1}, B: {2}".format(a, operator, b))

if operator == "add":
    print(str(a + b))
elif operator == "sub":
    print(str(a - b))
elif operator == "mul":
    print(str(a * b))
elif operator == "div":
    print(str(a / b))
else:
    print("Unsuported operator")



