# 5 philosophers, 5 chopsticks with deadlock

import threading
import time

noPortions = 10

class Chopstick:
    def __init__(self, id):
        self.id = id
        self.semaphore = threading.Semaphore()
        self.occupied = False
    
    def acquire(self):
        while self.occupied == True:
            pass

        print("Take chopstick {0}".format(self.id))

        self.semaphore.acquire()
        self.occupied = True

    def release(self):
        self.semaphore.release()
        self.occupied = False

class Philosopher:
    def __init__(self, id, lChopstick, rChopstick):
        self.leftChopstick = lChopstick
        self.rightChopstick = rChopstick
        self.id = id

    def process(self):
        for i in range(noPortions):
            self.leftChopstick.acquire()
            self.rightChopstick.acquire()
            print("Philosopher {0} is eating".format(self.id))
            time.sleep(0.5) # eat
            self.leftChopstick.release()
            self.rightChopstick.release()


chopsticks = []
for i in range(5):
    chopsticks.append(Chopstick(i))

philosophers = [Philosopher(0, chopsticks[0], chopsticks[1]), Philosopher(1, chopsticks[1], chopsticks[2]),
                Philosopher(2, chopsticks[2], chopsticks[3]),Philosopher(3, chopsticks[3], chopsticks[4]),
                Philosopher(4, chopsticks[4], chopsticks[0])]

threads = []

for i  in range(5):
    threads.append(threading.Thread(target=philosophers[i].process))
    threads[i].start()


