# 5 philosophers, 5 chopsticks without deadlock (Chndy/Misra solution)

import threading
import time

noPortions = 10

class Chopstick:
    def __init__(self, id):
        self.id = id
        self.semaphore = threading.Semaphore()
        self.occupied = None
        self.dirty = True
        self.requestId = None
        self.isUsed = False # Philosopher eats using this chopstick
        self.passState = False # Pass dirty or clean chopstick
    
    def acquire(self, phId):

        if self.occupied == phId:
            return True

        if self.isUsed == True:
            self.passState = True
        else:
            self.passState = False

        if self.occupied != None and self.dirty == False:
            print("Occupied {0}, dirty {1}".format(self.occupied, self.dirty))
            return False

        print("Take chopstick {0} by philosopher {1}".format(self.id, phId))
        
        self.semaphore.acquire()
        self.occupied = phId

        return True

    def release(self):
        self.semaphore.release()
        self.occupied = None
        self.dirty = self.passState
        self.passState = False

class Philosopher:
    def __init__(self, id, lChopstick, rChopstick):
        self.leftChopstick = lChopstick
        self.rightChopstick = rChopstick
        self.id = id

        self.leftChopstick.acquire(self.id)

    def takeChopsticks(self, lChopstick, rChopstick):
        while lChopstick.acquire(self.id) == False and rChopstick.acquire(self.id) == False:
            pass

    def eat(self):
        self.leftChopstick.isUsed = True
        self.rightChopstick.isUsed = True
        time.sleep(0.5) # eat
        self.leftChopstick.isUsed = False
        self.rightChopstick.isUsed = False
        self.leftChopstick.dirty = True
        self.rightChopstick.dirty = True

    def process(self):
        for i in range(noPortions):
            self.takeChopsticks(self.leftChopstick, self.rightChopstick)

            print("Philosopher {0} is eating {1} portion".format(self.id, i))
            self.eat()

            self.leftChopstick.release()
            self.rightChopstick.release()



chopsticks = []
for i in range(5):
    chopsticks.append(Chopstick(i))

philosophers = [Philosopher(0, chopsticks[0], chopsticks[1]), Philosopher(1, chopsticks[1], chopsticks[2]),
                Philosopher(2, chopsticks[2], chopsticks[3]),Philosopher(3, chopsticks[3], chopsticks[4]),
                Philosopher(4, chopsticks[4], chopsticks[0])]

threads = []

for i  in range(5):
    threads.append(threading.Thread(target=philosophers[i].process))
    threads[i].start()


